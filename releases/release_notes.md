**0.4 - 20170227**

* Added Jenkins plugin and extension mechanism
* Startup guard when FacesContext not yet available

**0.3 - 20160418**

* Logout without validation and only if logged in
* Correctly set remote repository (initially)
* Restart (only) on save when needed
* Log changes by username
* Logging to file (which some configuration options)

**0.2** 

* Ldap authentication

**0.1**

* Initial release

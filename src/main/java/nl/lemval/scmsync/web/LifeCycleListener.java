package nl.lemval.scmsync.web;

import javax.servlet.*;

import org.slf4j.*;

import nl.lemval.scmsync.auth.Authenticator;
import nl.lemval.scmsync.schedule.GitSyncScheduler;

/**
 * Listener for startup/shutdown of the web application.
 * It initializes the authenticator and starts the sync scheduler on startup
 * and stops the sync scheduler on shutdown.
 *
 * @author M. van Leeuwen
 */
public class LifeCycleListener implements ServletContextListener {

	private static final Logger Logger = LoggerFactory.getLogger(LifeCycleListener.class);

	@Override
	public void contextInitialized(ServletContextEvent event) {
		Logger.debug("Initializing context");
		Authenticator.getInstance();
		GitSyncScheduler.getInstance().start();
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		GitSyncScheduler.getInstance().stop();
		Logger.debug("Destroyed context");
	}
}

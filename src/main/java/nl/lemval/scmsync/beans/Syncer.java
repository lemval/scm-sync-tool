package nl.lemval.scmsync.beans;

import javax.annotation.PostConstruct;
import javax.faces.bean.*;

import nl.lemval.scmsync.auth.Session;
import nl.lemval.scmsync.schedule.GitSyncScheduler;
import nl.lemval.scmsync.task.GitSynchroniser;

/**
 * Syncer bean (eager).
 *
 * @author M. van Leeuwen
 */
@ManagedBean(name = "syncer", eager = true)
@SessionScoped
public class Syncer {

	private GitSyncScheduler scheduler;

	@PostConstruct
	public void init() {
		scheduler = GitSyncScheduler.getInstance();
	}

	/**
	 * Manually start a sync. User is picked from the sessioun.
	 *
	 * @return The redirect page to show.
	 */
	public String startSync() {
		GitSyncScheduler.getInstance().runOnce(Session.getUserName());
		return "sync?faces-redirect=true";
	}

	public String restart() {
		GitSyncScheduler.getInstance().restart();
		return null;
	}

	public String getStatus() {
		return scheduler.getStatus();
	}

	public boolean isFinished() {
		return scheduler.hasCompleted();
	}

	public void updateStatus() {
		// Nothing to do here
	}

	public String getSyncLog() {
		return GitSynchroniser.getLastLogs();
	}

	public String getSyncChanges() {
		return GitSynchroniser.getLastChangedFiles();
	}
}

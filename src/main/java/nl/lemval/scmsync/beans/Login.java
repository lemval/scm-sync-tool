package nl.lemval.scmsync.beans;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.*;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import nl.lemval.scmsync.auth.*;

/**
 * Login bean.
 *
 * @author M. van Leeuwen
 */
@ManagedBean
@SessionScoped
public class Login implements Serializable {

	private static final long serialVersionUID = 1L;

	private String password;
	private String user;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public boolean isAuthenticated() {
		final HttpSession session = Session.getSession();
		return session != null && session.getAttribute(Session.SESSION_USERNAME) != null;
	}

	public String validateLogin() {
		final User authenticatedUser = Authenticator.getInstance().login(user, password);
		final HttpSession session = Session.getSession();
		if (session != null && authenticatedUser != null) {
			session.setAttribute(Session.SESSION_USERNAME, authenticatedUser.getName());
			session.setAttribute(Session.SESSION_USERMAIL, authenticatedUser.getMail());
			return "index?faces-redirect=true";
		}

		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
				"Incorrect username and/or password.", "Please enter the correct username and password."));

		return "login";
	}

	public String logout() {
		Session.getSession().invalidate();
		Authenticator.getInstance().logout(user);
		return "login?faces-redirect=true";
	}
}

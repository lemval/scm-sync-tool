package nl.lemval.scmsync.beans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.*;

import it.sauronsoftware.cron4j.Predictor;
import nl.lemval.scmsync.task.SyncConfig;
import nl.lemval.scmsync.util.DateUtil;

/**
 * Configuration bean (eager).
 *
 * @author M. van Leeuwen
 */
@ManagedBean(name = "configuration", eager = true)
@ApplicationScoped
public class Configuration implements Serializable {
	private static final long serialVersionUID = 1L;

	private SyncConfig config;

	@PostConstruct
	private void init() {
		this.config = SyncConfig.getInstance();
	}

	public String getNextRun() {
		return DateUtil.format(new Predictor(getSchedule()).nextMatchingDate());
	}

	public String getConfigLocation() {
		return config.getPath();
	}

	public void setConfigLocation(String configLocation) {
		config.setPath(configLocation);
	}

	public String getSchedule() {
		return config.getSchedule();
	}

	public void setSchedule(String value) {
		config.setSchedule(value);
	}

	public String getRepository() {
		return config.getRepository();
	}

	public void setRepository(String value) {
		config.setRepository(value);
	}

	public String getFolder() {
		return config.getFolder();
	}

	public void setFolder(String value) {
		config.setFolder(value);
	}

	public String getInclude() {
		return config.getInclude();
	}

	public void setInclude(String value) {
		config.setInclude(value);
	}

	public String getExclude() {
		return config.getExclude();
	}

	public void setExclude(String value) {
		config.setExclude(value);
	}
}
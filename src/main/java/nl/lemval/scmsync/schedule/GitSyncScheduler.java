package nl.lemval.scmsync.schedule;

import java.util.List;

import org.slf4j.*;

import it.sauronsoftware.cron4j.*;
import nl.lemval.scmsync.plugins.*;
import nl.lemval.scmsync.task.SyncConfig;

/**
 * Scheduler (singleton) for the automatic starting of the sync task following
 * the configured cron schedule.
 *
 * @author M. van Leeuwen
 */
public class GitSyncScheduler {

	private static final String NEWLINE = System.getProperty("line.separator");
	private static final Logger Logger = LoggerFactory.getLogger(GitSyncScheduler.class);

	private static GitSyncScheduler instance;

	private Scheduler scheduler;
	private String taskIdentifier;
	private final SyncConfig config;
	private final List<ScmSyncPlugin> plugins;

	public static GitSyncScheduler getInstance() {
		if (instance == null) {
			instance = new GitSyncScheduler();
		}
		return instance;
	}

	private GitSyncScheduler() {
		config = SyncConfig.getInstance();
		plugins = new PluginRunner().initializePlugins();
	}

	/**
	 * Start the schedule following the cron schedule.
	 */
	public void start() {
		Logger.info("Starting the sync scheduler.");
		Logger.info("Using cron schedule: " + config.getSchedule());

		scheduler = new Scheduler();
		final GitSyncTask task = new GitSyncTask(plugins);
		taskIdentifier = scheduler.schedule(config.getSchedule(), task);
		scheduler.start();
	}

	/**
	 * Start the task once (manual invocation)
	 *
	 * @param user
	 *            The user starting the manual task.
	 */
	public void runOnce(String user) {
		Logger.info("Manually running the task.");
		final GitSyncTask task = new GitSyncTask(user, plugins);
		scheduler.launch(task);
	}

	/**
	 * Restart the scheduler after for example a configuration change.
	 */
	public void restart() {
		Logger.info("Restarting the sync scheduler...");
		scheduler.reschedule(taskIdentifier, config.getSchedule());
	}

	/**
	 * Query the status of all executing tasks.
	 *
	 * @return Status (multi-line) of all tasks.
	 */
	public String getStatus() {
		final StringBuilder sb = new StringBuilder();
		final TaskExecutor[] tasks = scheduler.getExecutingTasks();
		for (int i = 0; i < tasks.length; i++) {
			final TaskExecutor executor = tasks[i];
			if (executor.supportsStatusTracking()) {
				if (sb.length() > 0) {
					sb.append(NEWLINE);
				}
				sb.append("[").append(i).append("] ").append(NEWLINE);
				sb.append(executor.getStatusMessage());
			}
		}
		return sb.toString();
	}

	public void stop() {
		Logger.info("Stopping the sync scheduler.");
		if (scheduler != null) {
			scheduler.stop();
		}
	}

	/**
	 * Check if all tasks have been completed.
	 *
	 * @return True if no tasks are running.
	 */
	public boolean hasCompleted() {
		return scheduler.getExecutingTasks().length == 0;
	}
}

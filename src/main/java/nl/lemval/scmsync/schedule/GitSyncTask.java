package nl.lemval.scmsync.schedule;

import java.util.*;

import org.slf4j.*;

import it.sauronsoftware.cron4j.*;
import nl.lemval.scmsync.plugins.ScmSyncPlugin;
import nl.lemval.scmsync.task.*;
import nl.lemval.scmsync.util.DateUtil;

/**
 * Task to run the sync scheduler.
 *
 * @author M. van Leeuwen
 */
public class GitSyncTask extends Task {

	public interface MsgReceiver {
		void onMessage(final String message);
	}

	private static final Logger Logger = LoggerFactory.getLogger(GitSyncTask.class);
	private static final String NEWLINE = System.getProperty("line.separator");

	private final String invokingUser;
	private final HashMap<Integer, StringBuilder> builders;
	private final List<ScmSyncPlugin> plugins;

	public GitSyncTask(List<ScmSyncPlugin> plugins) {
		this(null, plugins);
	}

	/**
	 * Create a new synchronization task.
	 *
	 * @param user
	 *            The user invoking the sync process.
	 * @param plugins
	 *            List of plugins found (can be null)
	 */
	public GitSyncTask(final String user, final List<ScmSyncPlugin> plugins) {
		this.invokingUser = user;
		this.plugins = plugins;
		this.builders = new HashMap<>();
	}

	@Override
	public void execute(final TaskExecutionContext context) throws RuntimeException {
		final MsgReceiver messenger = message -> {
			StringBuilder sb = builders.get(context.hashCode());
			if (sb == null) {
				sb = new StringBuilder();
				builders.put(context.hashCode(), sb);
			} else {
				sb.append(NEWLINE);
			}
			sb.append(message);
			context.setStatusMessage(sb.toString());
			Logger.info("[TASK] " + message);
		};

		messenger.onMessage("Sync started at " + DateUtil.now());
		try {
			Logger.info("Sync process started");
			if (plugins != null) {
				for (final ScmSyncPlugin plugin : plugins) {
					plugin.execute(messenger);
				}
			}
			GitSynchroniser.performSync(invokingUser, messenger);
		} catch (final SyncException e) {
			Logger.error("Task has failed: " + e.getMessage());
			throw new RuntimeException(e);
		}
		messenger.onMessage("Task finished.");
	}

	@Override
	public boolean supportsStatusTracking() {
		return true;
	}
}

package nl.lemval.scmsync.auth;

import java.io.IOException;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.*;

import org.slf4j.*;

/**
 * Web filter so all requests are authorized.
 *
 * @author M. van Leeuwen
 */
@WebFilter(filterName = "AuthorizationFilter", urlPatterns = { "*.xhtml" })
public class AuthorizationFilter implements Filter {

	private static final Logger Logger = LoggerFactory.getLogger(AuthorizationFilter.class);

	private static final String LOGIN_PAGE = "/login.xhtml";

	public AuthorizationFilter() {
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		Logger.info("Initialized auth filter with " + filterConfig.toString());
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		try {

			final HttpServletRequest reqt = (HttpServletRequest) request;
			final HttpServletResponse resp = (HttpServletResponse) response;
			final HttpSession ses = reqt.getSession(false);

			final String reqURI = reqt.getRequestURI();
			if (reqURI.contains(LOGIN_PAGE) || (ses != null && ses.getAttribute(Session.SESSION_USERNAME) != null)
					|| reqURI.contains("/public/") || reqURI.contains("javax.faces.resource")
					|| Authenticator.getInstance().isLoggedIn()) {
				chain.doFilter(request, response);
			} else {
				resp.sendRedirect(reqt.getContextPath() + LOGIN_PAGE);
			}
		} catch (final Exception e) {
			Logger.error("Could not filter request due to " + e.getMessage(), e);
		}
	}

	@Override
	public void destroy() {
	}
}

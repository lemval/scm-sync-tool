package nl.lemval.scmsync.auth;

import java.io.Serializable;

/**
 * User domain object.
 *
 * @author M. van Leeuwen
 */
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String mail;
	private final String name;

	public User(String name, String mail) {
		this.name = name;
		this.mail = mail;
	}

	public String getName() {
		return name;
	}

	public String getMail() {
		return mail;
	}
}

package nl.lemval.scmsync.auth;

/**
 * Authenticator for anonymous access (no login required). User is logged as
 * 'anonymous'.
 *
 * @author M. van Leeuwen
 */
public class NoneProvider extends Authenticator {

	@Override
	public boolean isLoggedIn() {
		return true;
	}

	@Override
	public User login(String user, String password) {
		return ANONYMOUS_USER;
	}

	@Override
	public void logout(String user) {
	}
}

package nl.lemval.scmsync.auth;

import java.io.*;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.slf4j.*;

/**
 * Abstract class for authentication. Initializes it with either /etc/scm_auth
 * or $HOME/.scm_auth
 * The default provider is 'none'. Other options are 'ldap'.
 *
 * @author M. van Leeuwen
 */
public abstract class Authenticator {

	private static final Logger Logger = LoggerFactory.getLogger(Authenticator.class);

	public static final User ANONYMOUS_USER = new User("anonymous", "");

	private static Authenticator instance;

	public boolean isLoggedIn() {
		return false;
	}

	public abstract User login(String user, String password);

	public abstract void logout(String user);

	public static Authenticator getInstance() {
		if (instance != null) {
			return instance;
		}

		Logger.info("Initializing the authenticator...");

		try {
			instance = initialize();
		} catch (final Exception e) {
			throw new RuntimeException("Unable to create authenticator.", e);
		}
		return instance;
	}

	private static Authenticator initialize() throws IOException, AuthException {
		File path = setPath(System.getProperty("authConfiguration", "/etc/scm_auth"));
		if (path == null) {
			final String home = System.getProperty("user.home", ".");
			path = setPath(home + "/.scm_auth");
		}
		final Properties props = loadProperties(path);

		final String provider = props.getProperty("provider", "none").trim().toLowerCase();

		if ("ldap".equals(provider)) {
			Logger.info("Using LDAP provider.");
			return new LdapProvider(props);
		}
		Logger.info("Not using any provider.");
		return new NoneProvider();
	}

	private static Properties loadProperties(File path) throws IOException {
		final Properties authProperties = new Properties();
		FileReader reader = null;
		try {
			reader = new FileReader(path);
			authProperties.load(reader);
		} finally {
			IOUtils.closeQuietly(reader);
		}
		return authProperties;
	}

	public static File setPath(final String location) throws IOException {
		final File locationFile = new File(location);
		if (!locationFile.exists()) {
			try {
				locationFile.createNewFile();
			} catch (final IOException e) {
				// ignore
			}
		}
		if (locationFile.exists() && locationFile.canRead()) {
			return locationFile;
		}
		return null;
	}
}

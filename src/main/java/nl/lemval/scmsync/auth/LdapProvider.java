package nl.lemval.scmsync.auth;

import java.util.*;

import javax.naming.*;
import javax.naming.directory.*;

import org.slf4j.*;

import com.google.common.base.Splitter;

/**
 * Authenticator for LDAP based access. For properties, see readme.md.
 * <p>
 * The authenticator logs into LDAP using the managerDN credentials if
 * specified. It then checks
 * if the supplied user exists and can login using the supplied password. If
 * that succeeds, and
 * the allowed groups are specified, the groups of the users are validated. If
 * the user is in
 * one of the allowed groups, the login succeeds.
 * </p>
 * 
 * @author M. van Leeuwen
 */
public class LdapProvider extends Authenticator {

	private static final Logger Logger = LoggerFactory.getLogger(LdapProvider.class);

	private static final String CN = "cn";
	private static final String UID = "uid";

	private final String searchBase;
	private final String searchFilter;
	private final String serverUrl;
	private final String managerDn;
	private final String managerPassword;

	private final String groupBase;
	private final String groupFilter;
	private final String allowedGroups;

	private final String userName;
	private final String userMail;

	public LdapProvider(final Properties props) {
		this.searchBase = props.getProperty("ldap.searchBase", "");
		this.searchFilter = props.getProperty("ldap.searchFilter", "(uid={0})");
		this.userName = props.getProperty("ldap.userName", CN);
		this.userMail = props.getProperty("ldap.userMail", "mail");
		this.serverUrl = props.getProperty("ldap.url", "ldap://localhost:389");
		this.managerDn = props.getProperty("ldap.managerDN");
		this.managerPassword = props.getProperty("ldap.password");

		this.groupBase = props.getProperty("ldap.groupBase", "");
		this.groupFilter = props.getProperty("ldap.groupFilter", "(| (member={0}) (uniqueMember={0}) )");
		this.allowedGroups = props.getProperty("ldap.allowedGroups");

		Logger.info("LDAP server : " + serverUrl);
		Logger.info("LDAP base   : " + searchBase);
		Logger.info("LDAP filter : " + searchFilter);
		if (managerDn != null) {
			Logger.info("LDAP account: " + managerDn);
		}
	}

	@Override
	public User login(String user, String password) {
		try {
			final NamingEnumeration<SearchResult> results = findUsers(user);

			if (results == null || !results.hasMore()) {
				Logger.error("Login for user '" + user + "' failed. User unknown.");
			}

			while (results.hasMore()) {
				final SearchResult sr = results.next();
				final User authenticatedUser = authenticateUser(sr, password);
				if (authenticatedUser != null) {
					return userInGroups(sr.getNameInNamespace()) ? authenticatedUser : null;
				}
			}
		} catch (final NamingException nex) {
			Logger.error("LDAP query failure: " + nex.getMessage(), nex);
		}
		return null;
	}

	@Override
	public void logout(String user) {
		Logger.info("User '" + user + "' logged out.");
	}

	private User authenticateUser(SearchResult sr, String password) {
		final Properties env = ldapConnectProperties();
		final Attributes attrs = sr.getAttributes();
		final String cn = getValue(attrs, userName);
		final String ml = getValue(attrs, userMail);

		env.put(Context.SECURITY_PRINCIPAL, sr.getNameInNamespace());
		env.put(Context.SECURITY_CREDENTIALS, password);

		DirContext dc = null;
		try {
			dc = new InitialDirContext(env);
		} catch (final NamingException e) {
			Logger.error("Login for user '" + getValue(attrs, UID) + "' failed: " + e.getMessage());
			return null;
		} finally {
			close(dc);
		}

		Logger.info("Login for user '" + getValue(attrs, UID) + "' successful.");
		return new User(cn, ml);
	}

	private boolean userInGroups(String dn) {
		if (allowedGroups == null || allowedGroups.trim().isEmpty()) {
			// No groups specified, so user is allowed
			return true;
		}

		try {
			final NamingEnumeration<SearchResult> results = findGroups(dn);

			if (results == null || !results.hasMore()) {
				Logger.error("No groups found for user '" + dn + "'.");
			}

			final List<String> groupList = Splitter.on(',').omitEmptyStrings().trimResults().splitToList(allowedGroups);

			while (results.hasMore()) {
				final SearchResult sr = results.next();
				final String group = getValue(sr.getAttributes(), CN);
				if (groupList.contains(group)) {
					return true;
				}
				Logger.debug("User group '{}' not listed in {}.", group, groupList);
			}
		} catch (final NamingException nex) {
			Logger.error("LDAP query failure: " + nex.getMessage(), nex);
		}
		return false;
	}

	private NamingEnumeration<SearchResult> findUsers(String user) {
		return find(user, new String[] { UID, userName, userMail }, searchBase, searchFilter);
	}

	private NamingEnumeration<SearchResult> findGroups(String user) {
		return find(user, new String[] { CN }, groupBase, groupFilter);
	}

	private NamingEnumeration<SearchResult> find(String user, String[] attribs, String base, String filter) {
		final Properties env = ldapConnectProperties();

		if (managerDn != null && managerPassword != null) {
			env.put(Context.SECURITY_PRINCIPAL, managerDn);
			env.put(Context.SECURITY_CREDENTIALS, managerPassword);
		}

		DirContext dc = null;
		try {
			dc = new InitialDirContext(env);
			final SearchControls sc = new SearchControls();
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
			if (attribs != null) {
				sc.setReturningAttributes(attribs);
			}
			final String search = filter.replace("{0}", user);
			return dc.search(base, search, sc);
		} catch (final NamingException nex) {
			Logger.error("LDAP connection failure: " + nex.getMessage(), nex);
		} finally {
			close(dc);
		}
		return null;
	}

	private Properties ldapConnectProperties() {
		final Properties env = new Properties();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, serverUrl);
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		return env;
	}

	private String getValue(Attributes attrs, String key) {
		try {
			return (String) attrs.get(key).get();
		} catch (final Exception ignored) {
		}
		return null;
	}

	private void close(DirContext dc) {
		if (dc != null) {
			try {
				dc.close();
			} catch (final NamingException ignored) {
			}
		}
	}
}

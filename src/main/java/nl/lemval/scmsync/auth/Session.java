package nl.lemval.scmsync.auth;

import javax.faces.context.*;
import javax.servlet.http.*;

/**
 * Session domain object
 *
 * @author M. van Leeuwen
 */
public class Session {

	public static final String SESSION_USERNAME = "username";
	public static final String SESSION_USERMAIL = "usermail";

	public static HttpSession getSession() {
		if (getContext() == null) {
			return null;
		}
		return (HttpSession) getContext().getSession(false);
	}

	public static HttpServletRequest getRequest() {
		if (getContext() == null) {
			return null;
		}
		return (HttpServletRequest) getContext().getRequest();
	}

	public static String getUserName() {
		final HttpSession session = getSession();
		if (session != null) {
			final Object attribute = session.getAttribute(SESSION_USERNAME);
			if (attribute != null) {
				return attribute.toString();
			}
		}
		return Authenticator.ANONYMOUS_USER.getName();
	}

	private static ExternalContext getContext() {
		if (FacesContext.getCurrentInstance() == null) {
			return null;
		}
		return FacesContext.getCurrentInstance().getExternalContext();
	}
}
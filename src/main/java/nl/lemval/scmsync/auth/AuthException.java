package nl.lemval.scmsync.auth;

/**
 * @author M. van Leeuwen
 */
public class AuthException extends Exception {

	private static final long serialVersionUID = 1L;

	public AuthException(final String message) {
		super(message);
	}
}

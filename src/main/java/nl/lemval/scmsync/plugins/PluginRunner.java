package nl.lemval.scmsync.plugins;

import java.util.*;

import org.reflections.Reflections;
import org.slf4j.*;

public final class PluginRunner {

	private static final Logger Logger = LoggerFactory.getLogger(PluginRunner.class);

	public List<ScmSyncPlugin> initializePlugins() {
		final List<ScmSyncPlugin> pluginList = new ArrayList<>();
		final Reflections r = new Reflections("nl.lemval.scmsync.plugins");
		final Set<Class<? extends ScmSyncPlugin>> plugins = r.getSubTypesOf(ScmSyncPlugin.class);

		for (final Class<? extends ScmSyncPlugin> pc : plugins) {
			try {
				pluginList.add(pc.newInstance());
			} catch (final Exception e) {
				Logger.error("Registration of plugin class {} failed: {}", pc.getSimpleName(), e.getMessage());
				Logger.debug("Plugin failure reason", e);
			}
		}

		for (final ScmSyncPlugin plugin : pluginList) {
			Logger.info("Plugin '{}' registered.", plugin.getName());
		}

		return pluginList;
	}
}

package nl.lemval.scmsync.plugins;

import java.io.*;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.slf4j.*;
import org.w3c.dom.*;

import nl.lemval.scmsync.schedule.GitSyncTask.MsgReceiver;
import nl.lemval.scmsync.task.*;
import nl.lemval.scmsync.util.FileUtil;

/**
 * Creates an xml file of all plugins installed.
 *
 * @author M. Suringa
 */
public class JenkinsPluginList implements ScmSyncPlugin {

	private static final Logger Logger = LoggerFactory.getLogger(JenkinsPluginList.class);

	public JenkinsPluginList() {
	}

	@Override
	public String getName() {
		return "Jenkins Plugin List";
	}

	/**
	 * Collects all plugins from the plugin folder and writes those to a file
	 * called 'pluginConfig.xml'. If the folder 'plugins' does not exist, it
	 * will throw an exception.
	 *
	 * See ScmSyncPlugin#execute(MsgReceiver)
	 */
	@Override
	public void execute(MsgReceiver messenger) throws SyncException {

		Logger.info("Updating plugin configuration");
		final SyncConfig config = SyncConfig.getInstance();
		final String pluginConfig = config.getFolder() + "/pluginConfig.xml";
		final File pluginFolder = new File(config.getFolder() + "/plugins");
		FileUtil.checkAccessibleFolder(pluginFolder, messenger);

		try {
			final FileFilter fileFilter = new WildcardFileFilter("*.jpi");
			final File[] plugins = pluginFolder.listFiles(fileFilter);

			final DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			final DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			final Document doc = docBuilder.newDocument();
			final Element rootElement = doc.createElement("plugins");

			doc.appendChild(rootElement);

			for (final File plugin : plugins) {
				final String pluginName = plugin.getName().toString();
				final Element pluginElement = doc.createElement("plugin");
				pluginElement.appendChild(doc.createTextNode(pluginName));
				rootElement.appendChild(pluginElement);
			}

			final TransformerFactory transformerFactory = TransformerFactory.newInstance();
			final Transformer transformer = transformerFactory.newTransformer();

			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

			final DOMSource source = new DOMSource(doc);
			final StreamResult result = new StreamResult(new File(pluginConfig));

			transformer.transform(source, result);

			Logger.info("Done writing plugin config to %s.", pluginConfig);
		} catch (final ParserConfigurationException pce) {
			throw new SyncException("Error in parser configuration: " + pce.getMessage());
		} catch (final TransformerException tfe) {
			throw new SyncException("Error in transformation: " + tfe.getMessage());
		}
	}
}

package nl.lemval.scmsync.plugins;

import nl.lemval.scmsync.schedule.GitSyncTask.MsgReceiver;
import nl.lemval.scmsync.task.SyncException;

/**
 * Entry point for adding plugins to the SCM sync mechanism. Add your classes to
 * the classpath, in package 'nl.lemval.scmsync.plugins' and let them implement
 * this interface. Make however sure there is a public no-arg constructor!
 *
 * All plugins are run before the actual sync with GIT.
 *
 *
 * @author lemval
 */
public interface ScmSyncPlugin {
	/**
	 * @return The name of the sync plugin
	 */
	String getName();

	/**
	 * Runs the plugin. Any exception is caught, in order to prevent breaking
	 * the sync mechanism.
	 *
	 * @param messenger
	 *            Used for sending messages to the task
	 * @throws SyncException
	 *             In case something goes wrong
	 */
	void execute(final MsgReceiver messenger) throws SyncException;
}

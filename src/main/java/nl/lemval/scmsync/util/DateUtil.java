package nl.lemval.scmsync.util;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Used for date related functions.
 *
 * @author M. van Leeuwen
 */
public class DateUtil {

	private static SimpleDateFormat FORMAT = new SimpleDateFormat("HH:mm (yyyy.MM.dd)", Locale.getDefault());

	/**
	 * Format a date in the predefined format HH:mm (yyyy.MM.dd).
	 *
	 * @param date
	 *            Date to format
	 *
	 * @return String representation of the date
	 */
	public static String format(final Date date) {
		return date == null ? "-" : FORMAT.format(date);
	}

	/**
	 * @return The current date/time formatted using the predefined format.
	 */
	public static String now() {
		return format(new Date());
	}
}

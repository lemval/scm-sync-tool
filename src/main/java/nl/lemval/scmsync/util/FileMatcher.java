package nl.lemval.scmsync.util;

import java.io.File;
import java.util.HashMap;
import java.util.regex.*;

import org.slf4j.*;

public class FileMatcher {

	private static final Logger Logger = LoggerFactory.getLogger(FileMatcher.class);
	private static final String DEFAULT_INCLUDE = "default";
	private static final String DEFAULT_FOLDER = "folder";

	private final HashMap<String, Pattern> patternObjs;
	private final boolean isInclude;

	public FileMatcher(Iterable<String> patterns, boolean isInclude) {
		this.isInclude = isInclude;
		this.patternObjs = new HashMap<>();
		for (final String pattern : patterns) {
			Pattern compiled = null;
			try {
				compiled = Pattern.compile(pattern);
			} catch (final PatternSyntaxException e) {
				Logger.debug("Not a regular expression: " + pattern);
			}
			patternObjs.put(pattern, compiled);
		}
	}

	public String matchOn(File path) {

		if (isInclude && path.isDirectory()) {
			// By default all folders are included upon include.
			return DEFAULT_FOLDER;
		}

		final String pathname = path.getName();
		for (final String key : patternObjs.keySet()) {
			if (pathname.contains(key)) {
				return key;
			}
			final Pattern pattern = patternObjs.get(key);
			if (pattern != null && pattern.matcher(pathname).find()) {
				return key;
			}
		}

		// No match found here...

		if (isInclude && patternObjs.isEmpty()) {
			// Include is by default, unless includes are specified.
			return DEFAULT_INCLUDE;
		}

		// Excludes have no default
		return null;
	}

}

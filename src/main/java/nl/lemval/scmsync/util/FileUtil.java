package nl.lemval.scmsync.util;

import java.io.File;

import nl.lemval.scmsync.schedule.GitSyncTask.MsgReceiver;
import nl.lemval.scmsync.task.SyncException;

public final class FileUtil {

	public static void checkAccessibleFolder(final File folder, final MsgReceiver messenger) throws SyncException {
		if (folder.exists() && folder.isDirectory() && folder.canRead()) {
			return;
		}

		String message = "Unknown error";
		if (!folder.exists()) {
			message = String.format("Folder '%s' does not exist.", folder);
		} else if (!folder.isDirectory()) {
			message = String.format("Folder '%s' isn't a directory.", folder);
		} else if (!folder.canRead()) {
			message = String.format("Folder '%s' cannot be read.", folder);
		}
		if (messenger != null) {
			messenger.onMessage(message);
		}
		throw new SyncException(message);
	}

}

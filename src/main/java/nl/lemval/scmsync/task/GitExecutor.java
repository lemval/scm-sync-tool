package nl.lemval.scmsync.task;

import static nl.lemval.scmsync.task.GitExecutor.Cmd.ADD;
import static nl.lemval.scmsync.task.GitExecutor.Cmd.COMMIT;
import static nl.lemval.scmsync.task.GitExecutor.Cmd.CONFIG;
import static nl.lemval.scmsync.task.GitExecutor.Cmd.FETCH;
import static nl.lemval.scmsync.task.GitExecutor.Cmd.INIT;
import static nl.lemval.scmsync.task.GitExecutor.Cmd.LOG;
import static nl.lemval.scmsync.task.GitExecutor.Cmd.MERGE;
import static nl.lemval.scmsync.task.GitExecutor.Cmd.PUSH;
import static nl.lemval.scmsync.task.GitExecutor.Cmd.REMOTE;
import static nl.lemval.scmsync.task.GitExecutor.Cmd.SHOW;
import static nl.lemval.scmsync.task.GitExecutor.Cmd.STATUS;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.regex.*;

import org.apache.commons.io.IOUtils;
import org.slf4j.*;

import com.google.common.collect.*;

import nl.lemval.scmsync.schedule.GitSyncTask.MsgReceiver;

/**
 * Class for executing git commands.
 *
 * @author M. van Leeuwen
 */
public class GitExecutor {

	private static final Logger Logger = LoggerFactory.getLogger(GitExecutor.class);

	/** The various commands used exactly as mentioned but lowercased */
	public enum Cmd {
		CONFIG, INIT, STATUS, REMOTE, PUSH, FETCH, MERGE, COMMIT, ADD, LOG, SHOW;
	}

	/** The various git status fields as supplied by 'status'. */
	public enum Status {
		UNKNOWN("??"), ADDED("A"), MODIFIED("M"), ADD_MODIFIED("AM"), UNMERGED("UU");

		private String value;

		private Status(String value) {
			this.value = value;
		}

		public boolean equals(String othervalue) {
			return value.equals(othervalue);
		}
	}

	private final String gitCmd;
	private final String gitUser;
	private final String gitPass;
	private File folder;
	private MsgReceiver messenger;

	public GitExecutor(String gitCmd, String gitUser, String gitPass) {
		this.gitCmd = gitCmd;
		this.gitUser = gitUser;
		this.gitPass = gitPass;
	}

	public void setFolder(File folder) {
		this.folder = folder;
	}

	/**
	 * Sets the user name and email for Git and logs in if this is a http(s)
	 * repository url and the username is set.
	 * After that, an init is performed (which is idempotent) and the origin is
	 * set to the (new) repository url.
	 *
	 * @param scmUrl
	 *            The (new) repository url
	 */
	public void init(final String scmUrl) {
		Logger.debug("Initializing the git repository");

		final String host = findHostAddress();
		final String user = System.getProperty("user.name");

		exec(CONFIG, "user.email", user + "@" + host);
		exec(CONFIG, "user.name", "ScmSync on " + host);

		if (!gitUser.isEmpty() && (scmUrl.startsWith("http://") || scmUrl.startsWith("https://"))) {
			final String url = getHostPartOf(scmUrl);
			if (url != null) {
				exec(CONFIG, "credential.helper", "cache");
				exec(CONFIG, gitPass, false, "credential." + url + ".username", gitUser);
			}
		}
		exec(INIT, "--quiet");

		Logger.debug("Setting origin to " + scmUrl);

		// Always replace origin to make sure it reflects the latest setting.
		// set-url only works when the remote ('origin') is already added!
		exec(REMOTE, "remove", "origin");
		exec(REMOTE, "add", "origin", scmUrl);

		message("Git repository initialized.");
	}

	private void message(String msg) {
		if (messenger != null) {
			messenger.onMessage(msg);
		}
	}

	/**
	 * Update the local code from the remote as this might have been changed by
	 * another process.
	 */
	public void refresh() {
		message("Updating from remote...");

		final Result fetchStatus = exec(FETCH, "--prune", "--quiet");
		if (fetchStatus.getExit() != 0) {
			fetchStatus.print(Logger, "Fetch failure. Check your configuration! Using non-existing repository?");
		}

		final Result status = exec(MERGE, "--quiet", "origin/master");
		if (status.getExit() != 0) {
			message("No clean merge. Trying to recover...");
			Logger.warn("Merge failed. Trying to recover...");

			if (status.findError("commit your changes") || status.findError("you have unmerged files")) {
				commitChanges(getCurrentStatus(), "[Merge recovery] Commit local changes.");
				exec(MERGE, "--quiet", "origin/master").print(Logger, "Merge recovery on commit all.");
			} else {
				status.print(Logger, "Unknown how to recover!");
			}
		}

		final Multimap<String, String> currentStatus = getCurrentStatus();

		// There should not be any status. If so, either the previous run failed
		// or the repo was modified externally.
		// Merge conflicts are hard to do automatically. Not supported yet.

		if (!currentStatus.isEmpty()) {
			commitChanges(currentStatus,
					"Changes caused by refresh (modified due to manual tampering or a failed previous run)");
		}
		message("Updated.");
		Logger.debug("Updated the git repository");
	}

	/**
	 * Commit and push the local changes to the remote. When the user is not
	 * specified (null) it is assumed
	 * to be an automatic sync.
	 *
	 * @param user
	 *            The user who initiated the change (or null).
	 */
	public void commit(final String user) {
		final Multimap<String, String> currentStatus = getCurrentStatus();
		if (!currentStatus.isEmpty()) {
			commitChanges(currentStatus, user == null ? "Automatic sync" : "Manual sync by " + user);
		}
		// Push always as the refresh might have added a commit
		exec(PUSH, "--porcelain", "--quiet", "origin", "master");
		message("Changes pushed to the remote.");
		Logger.debug("Pushed the git repository");
	}

	private void commitChanges(Multimap<String, String> currentStatus, String commitMessage) {
		message("Committing changes: " + commitMessage);
		for (final String key : currentStatus.keySet()) {
			Logger.debug("On key {} and value {}", key, currentStatus.get(key));
			if (!Status.ADDED.equals(key)) {
				for (final String element : currentStatus.get(key)) {
					exec(ADD, element);
				}
			}
		}
		final Result status = exec(COMMIT, "--quiet", "-m", commitMessage);
		status.print(Logger, "Changes committed.");
	}

	private Multimap<String, String> getCurrentStatus() {
		final Multimap<String, String> result = ArrayListMultimap.create();
		final Result status = exec(STATUS, "--porcelain");
		final Pattern p = Pattern.compile("(..) (.*)");
		for (final String statusLine : status.getStdOut()) {
			Logger.debug("Working with line {}", statusLine);
			final Matcher m = p.matcher(statusLine);
			if (m.matches()) {
				result.put(m.group(1).trim(), m.group(2));
			}
		}
		return result;
	}

	private String findHostAddress() {
		try {
			return InetAddress.getLocalHost().getHostName();
		} catch (final UnknownHostException e) {
			return "unknown";
		}
	}

	private Result exec(Cmd command, String... arguments) {
		return exec(command, null, false, arguments);
	}

	private Result exec(Cmd command, String stdIn, boolean stderr, String... arguments) {
		Process process = null;
		StreamMonitor out = null;
		StreamMonitor err = null;
		StreamPusher in = null;
		int exit = -1;

		try {
			final List<String> cmd = new ArrayList<>();
			cmd.add(gitCmd);
			cmd.add(command.name().toLowerCase());
			for (final String arg : arguments) {
				cmd.add(arg);
			}

			Logger.debug("Command: {} [{}].", String.join(" ", cmd), folder);

			process = new ProcessBuilder(cmd).directory(folder).start();

			out = new StreamMonitor(process.getInputStream());
			err = new StreamMonitor(process.getErrorStream());
			if (stdIn != null && !stdIn.isEmpty()) {
				in = new StreamPusher(process.getOutputStream(), stdIn);
			}
			exit = process.waitFor();

			Logger.debug("Command exited with: {}", exit);

		} catch (final IOException | InterruptedException e) {
			final String msg = String.format("Execution of %s failed: %s", command.name(), e.getMessage());
			message(msg);
			Logger.error(msg, e);
		} finally {
			IOUtils.closeQuietly(out);
			IOUtils.closeQuietly(err);
			IOUtils.closeQuietly(in);

			process.destroy();
		}
		return new Result(exit, out.getDataBuffer(), err.getDataBuffer());
	}

	private String getHostPartOf(String scmUrl) {
		final Pattern p = Pattern.compile("(https?):\\/\\/(([^@]*)@([^\\/]*)|([^\\/]*)).*");
		final Matcher matcher = p.matcher(scmUrl);
		if (matcher.matches()) {
			if (matcher.group(3) == null) {
				return String.format("%s://%s", matcher.group(1), matcher.group(5));
			}
			Logger.error("Scm URL contains username and password. Please remove!");
		} else {
			Logger.error("Expecting http(s) URL but got {}.", scmUrl);
		}
		return null;
	}

	public String getLogs(int count) {
		return exec(LOG, "--decorate=full", "-" + count).toString();
	}

	public String getLastChangedFiles() {
		return exec(SHOW, "--stat", "--oneline", "HEAD").toString();
	}

	public void setMessenger(MsgReceiver messenger) {
		this.messenger = messenger;
	}
}

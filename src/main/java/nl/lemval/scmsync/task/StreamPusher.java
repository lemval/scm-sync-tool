package nl.lemval.scmsync.task;

import java.io.*;

import org.apache.commons.io.IOUtils;

/**
 * Stream pusher (stdin) in a separate thread.
 *
 * @author M. van Leeuwen
 */
public class StreamPusher implements Closeable {

	private final OutputStream outputStream;
	private final String inputData;
	private Thread thread;

	/**
	 * Creates a new pusher
	 *
	 * @param outputStream
	 *            The stdin of a process
	 * @param inputData
	 *            The data to push to the process
	 */
	public StreamPusher(OutputStream outputStream, String inputData) {
		this.outputStream = outputStream;
		this.inputData = inputData;
		runThread();
	}

	private void runThread() {
		thread = new Thread() {
			@Override
			public void run() {
				PrintWriter printWriter = null;
				try {
					printWriter = new PrintWriter(outputStream);
					printWriter.println(inputData);
					printWriter.flush();
				} finally {
					IOUtils.closeQuietly(printWriter);
				}
			}
		};
		thread.start();
	}

	@Override
	public void close() {
		try {
			thread.interrupt();
			thread.join();
		} catch (final InterruptedException ignored) {
		}
		IOUtils.closeQuietly(outputStream);
	}
}

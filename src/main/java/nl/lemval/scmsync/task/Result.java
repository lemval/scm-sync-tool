package nl.lemval.scmsync.task;

import java.util.Collection;

import org.slf4j.*;

/**
 * Represents the result of the execution of an external command
 *
 * @author M. van Leeuwen
 */
public class Result {

	private static final String newline = System.getProperty("line.separator");
	private static final Logger Logger = LoggerFactory.getLogger(Result.class);

	private final int exit;
	private final Collection<String> out;
	private final Collection<String> err;

	/**
	 * Create a new result
	 *
	 * @param exit
	 *            Command's exit code
	 * @param out
	 *            Command's standard out lines
	 * @param err
	 *            Command's standard error lines
	 */
	public Result(int exit, Collection<String> out, Collection<String> err) {
		this.exit = exit;
		this.out = out;
		this.err = err;

		logDebug();
	}

	private void logDebug() {
		if (Logger.isDebugEnabled()) {
			if (!out.isEmpty()) {
				Logger.debug("[out]: {}", out);
			}
			if (!err.isEmpty()) {
				Logger.debug("[err]: {}", err);
			}
		}
	}

	public int getExit() {
		return exit;
	}

	public Collection<String> getStdOut() {
		return out;
	}

	public Collection<String> getStdErr() {
		return err;
	}

	/**
	 * Logs this object to the logger, prepended with a message.
	 *
	 * @param log
	 *            The logger to write to
	 * @param message
	 *            The additonal message (may not be null)
	 */
	public void print(Logger log, String message) {
		if (exit != 0) {
			log.warn(message);
			log.warn(" ==> {}", exit);
		} else if (!(out.isEmpty() && err.isEmpty())) {
			log.info(message);
		}
		for (final String line : out) {
			log.warn(" ==> {}", line);
		}
		for (final String line : err) {
			log.warn(" ==> {}", line);
		}
	}

	/**
	 * Find a specific error message in stdout/stderr.
	 *
	 * @param error
	 *            The message to find
	 *
	 * @return True if the error is contained in the out/err.
	 */
	public boolean findError(String error) {
		for (final String line : out) {
			if (line.contains(error)) {
				return true;
			}
		}
		for (final String line : err) {
			if (line.contains(error)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @return The standard out (multiple lines)
	 */
	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer();
		for (final String line : getStdOut()) {
			sb.append(line).append(newline);
		}
		return sb.toString();
	}
}

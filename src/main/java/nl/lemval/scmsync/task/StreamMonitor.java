package nl.lemval.scmsync.task;

import java.io.*;
import java.util.*;

import org.apache.commons.io.IOUtils;

/**
 * Monitors a stream (stdout/stderr) in a separate thread.
 *
 * @author M. van Leeuwen
 */
public class StreamMonitor implements Closeable {

	private final InputStream inputStream;
	private Thread thread;
	private final List<String> dataBuffer;

	/**
	 * Create a new monitor
	 * 
	 * @param inputStream
	 *            The stderr or stdout of a process
	 */
	public StreamMonitor(InputStream inputStream) {
		this.inputStream = inputStream;
		this.dataBuffer = new ArrayList<>();
		runThread();
	}

	private void runThread() {
		thread = new Thread() {
			@Override
			public void run() {
				String line = null;
				BufferedReader bufferedReader = null;
				try {
					bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
					while ((line = bufferedReader.readLine()) != null) {
						dataBuffer.add(line);
					}
				} catch (final IOException e) {
					e.printStackTrace();
				} finally {
					IOUtils.closeQuietly(bufferedReader);
				}
			}
		};
		thread.start();
	}

	@Override
	public void close() {
		try {
			thread.interrupt();
			thread.join();
		} catch (final InterruptedException ignored) {
		}
		IOUtils.closeQuietly(inputStream);
	}

	/**
	 * Get content of stream
	 *
	 * @return Unmodifiable content of stream
	 */
	public Collection<String> getDataBuffer() {
		return Collections.unmodifiableList(dataBuffer);
	}
}

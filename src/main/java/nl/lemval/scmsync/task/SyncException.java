package nl.lemval.scmsync.task;

/**
 * @author M. van Leeuwen
 */
public class SyncException extends Exception {

	private static final long serialVersionUID = 1L;

	public SyncException(String message) {
		super(message);
	}

}

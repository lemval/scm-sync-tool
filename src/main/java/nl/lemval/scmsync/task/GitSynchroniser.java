package nl.lemval.scmsync.task;

import java.io.*;
import java.util.*;

import org.apache.commons.io.FileUtils;
import org.slf4j.*;

import com.google.common.base.Splitter;

import nl.lemval.scmsync.schedule.GitSyncTask.MsgReceiver;
import nl.lemval.scmsync.util.*;

/**
 * The synchronizer performs the actual synchronization task. This is a static
 * class.
 *
 * @author M. van Leeuwen
 */
public class GitSynchroniser {

	private static final String FOLDER_NAME = "scmsyncfld";
	private static final String TEMP_SYNC_FOLDER = "." + FOLDER_NAME;

	private static final String GIT_FOLDER = ".git";
	private static final String TEMP_SYNC_FOLDER_PATTERN = ".*/\\." + FOLDER_NAME + "/?.*";

	private static final Logger Logger = LoggerFactory.getLogger(GitSynchroniser.class);

	private GitSynchroniser() {
	}

	/**
	 * Performs the actual sync. It reads the configuration, checks for the
	 * validity of the sync folder, finds all relevant files, prepares the git
	 * repository and copies all files to the temporary sync folder.
	 *
	 * @param user
	 *            The user performing the sync (null = automatic)
	 * @param messenger
	 *            The receiver of messages
	 *
	 * @throws SyncException
	 *             When the folder to sync isn't valid
	 */
	public static void performSync(String user, MsgReceiver messenger) throws SyncException {
		final SyncConfig config = SyncConfig.getInstance();
		final String scmUrl = config.getRepository();
		final String include = config.getInclude();
		final String exclude = config.getExclude();
		final String folder = config.getFolder();

		final File folderToSync = new File(folder);
		FileUtil.checkAccessibleFolder(folderToSync, messenger);
		final Collection<File> files = collectFilesToSynchronize(folderToSync, include, exclude);

		messenger.onMessage(String.format("Collected %d files to synchronize.", files.size()));

		final GitExecutor exec = getGitExecutor(messenger);

		exec.init(scmUrl);
		exec.refresh();

		copyAllFiles(files, folderToSync, getSyncGitFolder());

		exec.commit(user);
	}

	private static File getSyncGitFolder() {
		final String folder = SyncConfig.getInstance().getFolder();
		final File gitFolder = new File(folder, TEMP_SYNC_FOLDER);
		gitFolder.mkdir();
		return gitFolder;
	}

	private static GitExecutor getGitExecutor(MsgReceiver messenger) {
		final SyncConfig config = SyncConfig.getInstance();
		final String gitCmd = config.getGitCommand();
		final String gitUser = config.getScmUser();
		final String gitPass = config.getScmPassword();

		final GitExecutor executor = new GitExecutor(gitCmd, gitUser, gitPass);
		executor.setFolder(getSyncGitFolder());
		executor.setMessenger(messenger);
		return executor;
	}

	private static Collection<File> collectFilesToSynchronize(File base, String include, String exclude) {
		final Splitter csvTrimmer = Splitter.on(',').omitEmptyStrings().trimResults();
		final FileMatcher includes = new FileMatcher(csvTrimmer.split(include), true);
		final FileMatcher excludes = new FileMatcher(csvTrimmer.split(exclude), false);
		final ArrayList<File> collected = new ArrayList<>();
		doCollect(collected, base, includes, excludes);
		return collected;
	}

	private static void doCollect(ArrayList<File> collected, File base, FileMatcher includes, FileMatcher excludes) {
		final File[] files = base.listFiles((FileFilter) pathname -> fileMatches(pathname, includes, excludes));
		for (final File file : files) {
			if (file.isDirectory()) {
				doCollect(collected, file, includes, excludes);
			} else {
				collected.add(file);
			}
		}
	}

	private static boolean fileMatches(File pathname, FileMatcher includes, FileMatcher excludes) {

		if (pathname.getPath().matches(TEMP_SYNC_FOLDER_PATTERN) || GIT_FOLDER.equals(pathname.getName())) {
			return false;
		}

		final String includedBy = includes.matchOn(pathname);
		final String excludedBy = excludes.matchOn(pathname);

		final boolean match = includedBy != null && excludedBy == null;

		if (Logger.isDebugEnabled()) {
			final StringBuilder sb = new StringBuilder();
			sb.append("File '").append(pathname.getPath()).append("' is ");
			if (match) {
				sb.append("included by ").append(includedBy).append(".");
			} else {
				if (excludedBy == null) {
					sb.append("excluded by specified includes.");
				} else {
					sb.append("excluded by ").append(excludedBy).append(".");
				}
			}
			Logger.trace(sb.toString());
		}
		return match;
	}

	private static void copyAllFiles(final Collection<File> files, final File srcFolder, final File destFolder) {
		for (final File srcFile : files) {
			final File destFile = new File(destFolder, srcFile.getPath().replace(srcFolder.getPath(), ""));
			try {
				FileUtils.copyFile(srcFile, destFile, true);
			} catch (final IOException e) {
				Logger.error(String.format("File '%s' cannot be copied: %s", srcFile.getName(), e.getMessage()), e);
			}
		}
	}

	/**
	 * Returns the last 4 logs on the git sync repository.
	 *
	 * @return last logs as string (with new lines)
	 */
	public static String getLastLogs() {
		return getGitExecutor(null).getLogs(4);
	}

	/**
	 * Returns the information about the last commit
	 *
	 * @return last commit as string (with new lines)
	 */
	public static String getLastChangedFiles() {
		return getGitExecutor(null).getLastChangedFiles();
	}
}

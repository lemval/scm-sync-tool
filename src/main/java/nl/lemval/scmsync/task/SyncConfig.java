package nl.lemval.scmsync.task;

import java.io.*;
import java.util.Properties;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.commons.io.*;
import org.slf4j.*;

import it.sauronsoftware.cron4j.SchedulingPattern;
import nl.lemval.scmsync.auth.Session;
import nl.lemval.scmsync.schedule.GitSyncScheduler;

/**
 * The actual configuration class of this application (singleton). Each setter
 * will immediately write the new configuration to disk. Changes on file system
 * will be lost. Stop your server first.
 *
 * @author M. van Leeuwen
 */
public class SyncConfig {

	private static final Logger Logger = LoggerFactory.getLogger(SyncConfig.class);

	/** All available configuration fields */
	private enum Field {
		EXCLUDES("exclude"), // Regexp or exact match of excludes (CSV)
		INCLUDES("include"), // Same. Processed before the excludes.
		REPOSITORY("repo"), // The SCM repository to sync to.
		SYNCFOLDER("folder"), // The folder to synchronize
		SCMUSER("user"), // Optional user for push/pull
		SCMPASS("password"), // Optional password
		GITCOMMAND("gitcmd", "git"), // Executable for GIT
		SCHEDULE("schedule", "*/5 * * * *"), // The sync schedule
		;

		private final String key;
		private final String def;

		Field(final String key) {
			this.key = key;
			this.def = "";
		}

		Field(final String key, final String def) {
			this.key = key;
			this.def = def;
		}

		public String getKey() {
			return key;
		}

		public String getDefault() {
			return def;
		}
	}

	private static SyncConfig instance;

	private File configFile;
	private Properties configProperties;

	/**
	 * Get a handle of the configuration
	 *
	 * @return The configuration object
	 */
	public static SyncConfig getInstance() {
		if (instance == null) {
			instance = new SyncConfig();
			instance.initialize();
		}
		return instance;
	}

	private void initialize() {
		setPath(System.getProperty("syncConfiguration", "/etc/scm_sync"));
		if (configFile == null) {
			final String home = System.getProperty("user.home", ".");
			setPath(home + "/.scm_sync");
		}
	}

	private void loadProperties() {
		FileReader reader = null;
		try {
			configProperties = new Properties();
			reader = new FileReader(configFile);
			configProperties.load(reader);
		} catch (final IOException e) {
			throwMessage("Could not load property file: " + e.getMessage());
		} finally {
			IOUtils.closeQuietly(reader);
		}
	}

	private void writeProperties() {
		if (configProperties == null) {
			return;
		}
		final String comments = "Configuration for ScmSync is automatically updated.";

		FileWriter writer = null;
		try {
			writer = new FileWriter(configFile);
			configProperties.store(writer, comments);
		} catch (final IOException e) {
			throwMessage("Could not write property file: " + e.getMessage());
		} finally {
			IOUtils.closeQuietly(writer);
		}
	}

	/**
	 * Retrieve a property
	 *
	 * @param key
	 *            The key name
	 *
	 * @return Either the value in the configuration or the default (which might
	 *         be null)
	 */
	public String getProperty(Field key) {
		if (configProperties == null) {
			loadProperties();
		}
		return configProperties.getProperty(key.getKey(), key.getDefault());
	}

	/**
	 * Set a property.
	 *
	 * @param key
	 *            The key name
	 * @param value
	 *            The new value (cannot be set to null)
	 * @return True if the property actually changed
	 */
	public boolean setProperty(final Field key, final String value) {
		if (configProperties == null) {
			loadProperties();
		}

		final String current = configProperties.getProperty(key.getKey());
		if (current == null || current.equals(value) == false) {
			Logger.info("User {} changed property {} to {}", Session.getUserName(), key, value);

			configProperties.setProperty(key.getKey(), value);
			writeProperties();
			return true;
		}
		return false;
	}

	/**
	 * Get the path to the configuration file
	 *
	 * @return The absolute path of the configuration file
	 */
	public String getPath() {
		return configFile.getAbsolutePath();
	}

	/**
	 * Set the new path of the configuration file. If the file exists, the new
	 * file will be used. If the file does not exist, it is created (if
	 * possible) and filled with the data of the previous configuration file. On
	 * failure, the old file location is kept.
	 *
	 * @param location
	 *            The new location.
	 */
	public void setPath(final String location) {
		final String currentFile = configFile == null ? "<not set>" : configFile.getPath();

		final File locationFile = new File(location);
		if (!locationFile.exists()) {
			try {
				locationFile.createNewFile();
			} catch (final Exception e) {
				// ignore
			}
			if (configFile != null && locationFile.exists() && locationFile.canRead()) {
				try {
					FileUtils.copyFile(configFile, locationFile);
				} catch (final Exception e) {
					throwMessage("Copy config file failed: " + e.getMessage());
				}
			}
		}
		if (locationFile.exists() && locationFile.canRead()) {
			this.configFile = locationFile;
			this.configProperties = null;

			if (locationFile != null && currentFile.equals(location) == false) {
				Logger.info("User {} changed configuration file from {} to {}", Session.getUserName(), currentFile,
						location);
			}
		} else {
			Logger.debug("File {}: exists: {}, readable: {}.", locationFile, locationFile.exists(),
					locationFile.canRead());
			throwMessage("Non-existing or non-readable config file: " + locationFile);
		}
	}

	public String getSchedule() {
		return getProperty(Field.SCHEDULE);
	}

	public String getRepository() {
		return getProperty(Field.REPOSITORY);
	}

	public String getFolder() {
		return getProperty(Field.SYNCFOLDER);
	}

	public String getInclude() {
		return getProperty(Field.INCLUDES);
	}

	public String getExclude() {
		return getProperty(Field.EXCLUDES);
	}

	public String getGitCommand() {
		return getProperty(Field.GITCOMMAND);
	}

	public String getScmUser() {
		return getProperty(Field.SCMUSER);
	}

	public String getScmPassword() {
		return getProperty(Field.SCMPASS);
	}

	public void setSchedule(String schedule) {
		if (SchedulingPattern.validate(schedule)) {
			if (setProperty(Field.SCHEDULE, schedule)) {
				GitSyncScheduler.getInstance().restart();
			}
		} else {
			throwMessage("The cron pattern is invalid; the previous one has been restored.");
		}
	}

	private void throwMessage(String message) {
		final FacesContext context = FacesContext.getCurrentInstance();
		// Upon startup, the context might not be there.
		if (context != null) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, message, message));
		} else {
			Logger.warn(message);
		}
	}

	public void setRepository(String value) {
		setProperty(Field.REPOSITORY, value);
	}

	public void setFolder(String value) {
		setProperty(Field.SYNCFOLDER, value);
	}

	public void setInclude(String value) {
		setProperty(Field.INCLUDES, value);
	}

	public void setExclude(String value) {
		setProperty(Field.EXCLUDES, value);
	}

	public void setGitCommand(String value) {
		setProperty(Field.GITCOMMAND, value);
	}

	public void setScmUser(String value) {
		setProperty(Field.SCMUSER, value);
	}

	public void setScmPassword(String value) {
		// TODO Store encrypted
		setProperty(Field.SCMPASS, value);
	}
}

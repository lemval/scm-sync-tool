package nl.lemval.scmsync.plugins;

import nl.lemval.scmsync.schedule.GitSyncTask.MsgReceiver;
import nl.lemval.scmsync.task.SyncException;

public class FaultyPlugin implements ScmSyncPlugin {

	private FaultyPlugin() {
	}

	@Override
	public String getName() {
		return "Not good";
	}

	@Override
	public void execute(MsgReceiver messenger) throws SyncException {
	}
}

package nl.lemval.scmsync.plugins;

import java.util.List;

import org.mockito.*;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.*;
import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.Appender;
import junit.framework.*;

@SuppressWarnings({ "rawtypes", "unchecked" })
public abstract class LogTestCase extends TestCase {

	private final Appender mockedAppender;

	public LogTestCase() {
		mockedAppender = Mockito.mock(Appender.class);
		final Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
		logger.addAppender(mockedAppender);
	}

	protected void assertLogging(final String find, final Level level) {
		// verify using ArgumentCaptor
		final ArgumentCaptor<Appender> argumentCaptor = ArgumentCaptor.forClass(Appender.class);
		Mockito.verify(mockedAppender, Mockito.atLeastOnce()).doAppend(argumentCaptor.capture());

		boolean contains = false;
		final List<?> allValues = argumentCaptor.getAllValues();
		for (final Object ap : allValues) {
			final LoggingEvent event = (LoggingEvent) ap;
			final String message = event.getMessage();
			if (message.contains(find)) {
				contains = event.getLevel().equals(level);
			}
		}
		Assert.assertTrue("Logging does not contain: '" + find + "' on level " + level, contains);
	}
}

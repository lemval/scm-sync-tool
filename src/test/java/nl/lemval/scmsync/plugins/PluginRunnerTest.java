package nl.lemval.scmsync.plugins;

import java.util.List;

import ch.qos.logback.classic.Level;

public class PluginRunnerTest extends LogTestCase {

	public void testInitializePlugins() throws Exception {
		final PluginRunner target = new PluginRunner();
		final List<ScmSyncPlugin> actual = target.initializePlugins();
		assertEquals(1, actual.size());
		assertEquals("Jenkins Plugin List", actual.get(0).getName());
		assertLogging("Registration of plugin class {} failed", Level.ERROR);
	}

}

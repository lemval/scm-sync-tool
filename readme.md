
SCM Sync Tool
=============

What is the scm sync tool?
--------------------------

The SCM sync tool is created to solve the problem the scm-sync-plugin in Jenkins also tries to solve.

However, unlike the Jenkins' scm-sync-plugin, it does not work as a plugin, but as an additional application inside the container in which Jenkins is also running. 

The SCM sync tool allows for synchronisation of a single specific folder with a GIT repository. By means of includes and excludes the set of files can be selected. The synchronisation happens manual, scheduled or both.

Installation
------------

Place the WAR file into your container.

Or checkout the code, install JDK8 and Maven, and build it yourself. 

Configuration
-------------

Via the home page of the application you can set certain variables. Some need to be supplied manually:
* *user* - For non-ssh connections the username (don't include in repo URL)
* *password* - For non-ssh connections the password (don't include in repo URL)
* *git* - The full path to the git executable (default: git)

The variables are stored in either the file denoted by system property `syncConfiguration`, 
the file `/etc/scm_sync` or `$HOME/.scm_sync`. You can modify this later via the home page.

For logging, both standard out (warnings and up) and file logging is enabled using Logback.
You have the option to set system property `logback.configurationFile` for the location or
`syncLogFolder` for the location the log files are written (daily over two weeks, total
limited to 100MB). Note to terminate this with a slash.

Include/Exclude
---------------

By default everything is included. As soon as you specify an include pattern (comma separated, so it is hard to use commas in your pattern...)
all **files** matching the pattern in any folder are included. Any other **file** is excluded.

The exclude patterns follow after the include check and also match **folders**. By default nothing is excluded.
 
A match is valid if the pathname contains the specified string as text or adheres to the specified string as regular expression **find** in
this particular order.

Plugin support
--------------

Extend your synchronisation with own plugins by creating an implementation for the interface 'lemval.scmsync.plugins.ScmSyncPlugin'. Currently they need to be placed in the same package. On start of the application, the plugins will be initialized and upon each synchronisation, the plugin will run *before* the folder is committed and pushed to git.

The JenkinsPluginList plugin will always run. It will silently exit if there is nothing to do.

Example:
```java
public class ExamplePlugin implements ScmSyncPlugin {
	@Override
	public String getName() { return "Example Plugin"; }

	@Override
	public void execute(MsgReceiver messenger) throws SyncException {
		final SyncConfig config = SyncConfig.getInstance();
		try {
      // Your code here
		} catch (final Exception e) {
			throw new SyncException("Error in plugin: " + e.getMessage());
		}
	}
}
```

Authentication
--------------

Currently the following auth methods are supplied: none, ldap.

**None**

No authentication needed. Manual syncs are logged as anonymous.

**LDAP**

Your `/etc/scm_auth` (or `$HOME/.scm_auth`) should contain the following:

```
provider=ldap

ldap.url=ldap://hostname:389
#ldap.managerDN=cn=admin,dc=example,dc=com
#ldap.password=password

#ldap.searchBase=dc=example,dc=com
#ldap.searchFilter=(uid={0})
#ldap.userName=cn
#ldap.userMail=mail

#ldap.groupBase=dc=example,dc=com
#ldap.groupFilter=(| (member={0}) (uniqueMember={0}) )
#ldap.allowedGroups=
```
Without managerDN or password the login to the server will be anonymous. Without base, the search will be
performed on root level. Other fields are showing the defaults.

A sample configuration file:
```
#Configuration for ScmSync is automatically updated.
#Tue Mar 29 17:21:19 CEST 2016
include=\\.xml$
exclude=target
repo=git@gitlab.com\:group/repo.git
folder=/Users/lemval/workspace/scm_sync
schedule=*/5 * * * *
```
